$(function(){
		  $('[data-toggle="popover"]').popover();
		  $('.carousel').carousel({
			interval: 2000
			})
		  $('.modal').on('show.bs.modal',function(){
		  	console.log('Abriendo el modal')
		  	$('.button-modal').attr({'disabled':true});
		  	$('.button-modal').removeClass('btn-outline-light');
		  	$('.button-modal').addClass('btn-outline-danger');
		  })
		  $('.modal').on('shown.bs.modal',function(){
		  	console.log('Temino de abrirse el modal')
		  })
		  $('.modal').on('hide.bs.modal',function(){
		  	console.log('Empezando a cerrarse el modal')
		  })
		  $('.modal').on('hidden.bs.modal',function(){
		  	console.log('Modal cerrado')
		  	$('.button-modal').attr({'disabled':false});
		  	$('.button-modal').removeClass('btn-outline-danger');
		  	$('.button-modal').addClass('btn-outline-light');
		  })
		});